/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : MATERIAL LISTER AND HANDLER FOR THE RPG ENGINE/EDITOR    */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : materialbase.cpp                                         */
/*  User Interface: materialbase.ui                                          */
/*                                                                           */
/*****************************************************************************/


/* #### Language Header Files #### */
#include <dirent.h>
#include <stdio.h>
#include <iostream>

/* #### QLibrary Header Files #### */
#include <QMessageBox>
#include <QListWidget>

/* #### User Header Files     #### */
#include "materialbase.h"
#include "ui_materialbase.h"

/* #### Namespasces and variables #### */
std::string pathys[14];
std::string imagePathys[20][256];

std::string projectPathy;

// Constructor
materialbase::materialbase(QWidget *parent) : QDialog(parent),ui(new Ui::materialbase)
{
    ui->setupUi(this);
}

//Destructor
materialbase::~materialbase()
{
    delete ui;
}

void materialbase::on_pushButton_5_clicked()
{
    this->close();
}


void materialbase::SetUpFiles(QString PathToProject)
{
    std::string pathy = PathToProject.toStdString();

    // All paths set to Defined variables, to avoid conflict in case users define their personal folders.
    pathys[0] = pathy + ANIMATIONS;
    pathys[1] = pathy + AUTOTILES;
    pathys[2] = pathy + BACKGROUND;
    pathys[3] = pathy + BATTLERS;
    pathys[4] = pathy + CHARACTERS;
    pathys[5] = pathy + FOGS;
    pathys[6] = pathy + FONTS;
    pathys[7] = pathy + GUI;
    pathys[8] = pathy + ICONS;
    pathys[9] = pathy + IMAGES;
    pathys[10] = pathy + PORTRAITS;
    pathys[11] = pathy + TILESETS;
    pathys[12] = pathy + TITLES;
    pathys[13] = pathy + TRANSITIONS;

    ui->DirectoryList->insertItem(0, "Animations");
    ui->DirectoryList->insertItem(1, "Autotiles");
    ui->DirectoryList->insertItem(2, "Background");
    ui->DirectoryList->insertItem(3, "Battlers");
    ui->DirectoryList->insertItem(4, "Characters");
    ui->DirectoryList->insertItem(5, "Fogs");
    ui->DirectoryList->insertItem(6, "Fonts");
    ui->DirectoryList->insertItem(7, "GUI");
    ui->DirectoryList->insertItem(8, "Icons");
    ui->DirectoryList->insertItem(9, "Images");
    ui->DirectoryList->insertItem(10, "Portraits");
    ui->DirectoryList->insertItem(11, "Tilesets");
    ui->DirectoryList->insertItem(12, "Titles");
    ui->DirectoryList->insertItem(13, "Transitions");

    int j = 0;

    for(int i = 0; i < 14; i++)
    {
        const char* path = pathys[i].c_str();

        DIR *dir = opendir(path);

        counts[i] = 0;

        if (!dir)
        {
            QMessageBox msgBox;
            msgBox.setText("Material::SetUpFiles Error! Directory for Loading Graphics Data not found!");
            msgBox.exec();
          return;
        }

        struct dirent *entry = readdir(dir);

        j = 0;

        while (entry != NULL)
        {
            if (entry->d_type != DT_DIR)
            {
                imagePathys[i][j] = entry->d_name;
                counts[i]++;
            }
            j++;
            entry = readdir(dir);
        }
    }

}

void materialbase::on_DirectoryList_currentRowChanged(int currentRow)
{

    ui->ItemsList->clear();

    const char* path = pathys[currentRow].c_str();

    DIR *dir = opendir(path);

    if ( !dir )
    {
        QMessageBox msgBox;
        msgBox.setText("File Directory was not found!\nIf you see this and you are not Drakonchik, please let Drakonchik know this thing showed up!");
        msgBox.exec();
      return;
    }

    struct dirent *entry = readdir(dir);

    int i = 0;

    while (entry != NULL)
    {
        if (entry->d_type != DT_DIR)
        {
            ui->ItemsList->insertItem(i, entry->d_name);
        }
        i++;
        entry = readdir(dir);
    }
}

std::string materialbase::GetFileNameFromMB(int i, int j)
{
    return imagePathys[i][j];
}

std::string materialbase::GetFilePathFromMB(int i, int j)
{
    return pathys[i] + imagePathys[i][j];
}

void materialbase::on_ButtonPreview_clicked()
{

}
