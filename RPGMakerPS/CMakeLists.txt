cmake_minimum_required(VERSION 3.5)

project(RPGMakerPS LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check http://doc.qt.io/qt-5/deployment-android.html for more information.
# They need to be set before the find_package(Qt5 ...) call.

#if(ANDROID)
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
#    if (ANDROID_ABI STREQUAL "armeabi-v7a")
#        set(ANDROID_EXTRA_LIBS
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
#    endif()
#endif()

find_package(Qt5 COMPONENTS Widgets REQUIRED)

if(ANDROID)
  add_library(RPGMakerPS SHARED
    main.cpp
    rpgmakerps2.cppWhen executing step "CMake Build"
    rpgmakerps2.h
    rpgmakerps2.ui
    database.cpp
    database.h
    database.ui
    newproject.cpp
    newproject.h
    newproject.ui
    options.cpp
    options.h
    options.ui
    materialbase.cpp
    materialbase.h
    materialbase.ui
    materialimagedisplayer.cpp
    materialimagedisplayer.h
    materialimagedisplayer.ui
    stringblob.cpp
    stringblob.h
    newtilemap.cpp
    newtilemap.h
    newtilemap.ui
    tilemaps.cpp
    tilemaps.h
    newevent.cpp
    newevent.h
    newevent.ui
    maptile.h
    eventtile.h
    maptile.cpp
    eventtile.cpp
  )
else()
  add_executable(RPGMakerPS
    main.cpp
    rpgmakerps2.cpp
    rpgmakerps2.h
    rpgmakerps2.ui
    database.cpp
    database.h
    database.ui
    newproject.cpp
    newproject.h
    newproject.ui
    options.cpp
    options.h
    options.ui
    materialbase.cpp
    materialbase.h
    materialbase.ui
    materialimagedisplayer.cpp
    materialimagedisplayer.h
    materialimagedisplayer.ui
    stringblob.cpp
    stringblob.h
    newtilemap.cpp
    newtilemap.h
    newtilemap.ui
    tilemaps.cpp
    tilemaps.h
    newevent.cpp
    newevent.h
    newevent.ui
    maptile.h
    eventtile.h
    maptile.cpp
    eventtile.cpp
  )
endif()

target_link_libraries(RPGMakerPS PRIVATE Qt5::Widgets)
