/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : MAIN FORM                                                */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : rpgmakerps2.cpp                                          */
/*  User Interface: rpgmakerps2.ui                                           */
/*                                                                           */
/*****************************************************************************/

/* #### Language Header Files #### */
#include <iostream>
#include <fstream>
#include <stdlib.h>

/* #### QLibrary Header Files #### */
#include <QFileDialog>
#include <QFileInfo>
#include <QPixmap>
#include <QMessageBox>
#include <QGraphicsItem>



/* #### User Header Files     #### */
#include "rpgmakerps2.h"
#include "./ui_rpgmakerps2.h"
#include "database.h"
#include "newproject.h"
#include "options.h"
#include "maptile.h"

/* #### Namespasces and variables #### */
using namespace std;

int combinedTilesetCoordinates = 0;

string directoryPaths[20];
string imagePaths[20][512];
string dataPaths[32];

string mapNames[512];
int mapCount;

string configurationPath;

MapTile* Tile[512];
MapTile* TileMapItems[4096];

int tileCountW = 0;
int tileCountH = 0;
int selectedTilesetTileNumber = 0;


// Construtor
RPGMakerPS2::RPGMakerPS2(QWidget *parent) : QMainWindow(parent) , ui(new Ui::RPGMakerPS2)
{
    //Sets default name for the project so we avoid any errors when doing string related operations.
    ui->setupUi(this);
    projectName = "dummy";

    // Loading all the paths for the RPG enginese and the Emulators used for the operating of the RPG Maker PS
    std::ifstream configfile ("paths.config");
    std::string line;
    if (configfile.is_open())
    {
        std::getline(configfile,line);
        pcsxPath = line;
        std::getline(configfile,line);
        pcsx2Path = line;
        std::getline(configfile,line);
        SourcePathPS1 = line;
        std::getline(configfile,line);
        SourcePathPS2 = line;
        configfile.close();
    }

    mapCount = 0;

    //datty.setProjectName(QString::fromStdString(projectName),QString::fromStdString(projectPath));
    //datty.loadData(projectPath);
    datty.setModal(true);

    // Custom Context Menu for the Map List, giving ability to add new Map or Edit the existing one.
    ui->MapList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(QPoint)),this, SLOT(on_MapList_customContextMenuRequested(QPoint)));

    TileMapListMenu=new QMenu(this);
    TileMapListMenu->setTitle("Map Menu");

    NewTileMap = new QAction("New Map", this);
    ImportTileMap = new QAction("Import TMX Map", this);
    EditTileMap = new QAction("Edit Map(Selected)", this);
    DeleteTileMap = new QAction("Delete Map(Selected)", this);

    TileMapListMenu->addAction(NewTileMap);
    TileMapListMenu->addAction(ImportTileMap);
    TileMapListMenu->addAction(EditTileMap);
    TileMapListMenu->addAction(DeleteTileMap);

    connect(NewTileMap, &QAction::triggered, this, &RPGMakerPS2::CreateNewTileMap);
    connect(ImportTileMap, &QAction::triggered, this, &RPGMakerPS2::ImportTiledTileMap);
    connect(EditTileMap, &QAction::triggered, this, &RPGMakerPS2::EditSelectedTileMap);
    connect(DeleteTileMap, &QAction::triggered, this, &RPGMakerPS2::DeleteSelectedTileMap);

    tileMapForm.setModal(true);
}

// Destructor
RPGMakerPS2::~RPGMakerPS2()
{
    delete ui;
}

// This part here is executed when on the top bar you click the Database
void RPGMakerPS2::on_actionDatabase_triggered()
{
    datty.exec();
}

// This here is what makes a new project and allows you to make your project
void RPGMakerPS2::on_actionNew_triggered()
{
    NewProject projecty;
    projecty.setModal(true);

    // This lacks any checks yet for seeing if the file path already exists. Be cautious!

    if(projecty.exec() == QDialog::Accepted)
    {
        projectName = projecty.pathText;
        int versionnumber = projecty.version;
        string combined;
        switch (versionnumber)
        {
            case 0://PS1
            {
                combined= "mkdir -p ~/RPGMakerPS1Projects/" + projectName;
                system(combined.c_str());
                combined = "cp -R " + SourcePathPS1 + "/* ~/RPGMakerPS1Projects/" + projectName;
                system(combined.c_str());
                combined = "touch ~/RPGMakerPS1Projects/" + projectName + "/" + projectName + ".rpgps1";
                system(combined.c_str());
                std::string home = getenv("HOME");
                projectPath = "" + home + "/RPGMakerPS1Projects/" + projectName + "/";
            }
            break;
            case 1: // PS2
                combined = "mkdir -p ~/RPGMakerPS2Projects/" + projectName;
                system(combined.c_str());
                combined = "cp -R " + SourcePathPS2 + "/* ~/RPGMakerPS2Projects/" + projectName;
                system(combined.c_str());
                combined = "touch ~/RPGMakerPS2Projects/" + projectName + "/" + projectName + ".rpgps2";
                system(combined.c_str());
                std::string homey = getenv("HOME");
                projectPath = "" + homey + "/RPGMakerPS2Projects/" + projectName + "/";
            break;
        }

        datty.setProjectName(QString::fromStdString(projectName),QString::fromStdString(projectPath));
        datty.loadData(projectPath);

        matty.SetUpFiles(QString::fromStdString(projectPath));
        matty.setModal(true);
    }

}


// This here is what makes an opened project act as intended and load all data needed for the project opperation.
void RPGMakerPS2::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,"Open RPG Maker PS2 Project", "~",
            tr("RPGPS(*.rpgps2);;All Files (*)"));
    QFileInfo fi(fileName);
    projectName = fi.baseName().toStdString();
    std::string home = getenv("HOME");
    projectPath = "" + home + "/RPGMakerPS2Projects/" + projectName + "/";

    datty.setProjectName(QString::fromStdString(projectName),QString::fromStdString(projectPath));
    datty.loadData(projectPath);
    matty.SetUpFiles(QString::fromStdString(projectPath));
    matty.setModal(true);

    // Loading map names to the Editor
    std::ifstream mapnamefile (projectPath + "Data/MapNames.dat");
    std::string line;

    if (mapnamefile.is_open())
    {
        while (getline(mapnamefile,line))
        {
            TileMapHandler.SetTileMap(line, projectPath + "Data/Maps");
            ui->MapList->addItem(QString::fromStdString(line));
            mapNames[mapCount] = line;
            mapCount++;
        }
        mapnamefile.close();
    }
}

void RPGMakerPS2::on_actionPlaytest_Emulator_triggered()
{
    string texty = "" + pcsx2Path + " --elf=\"~/RPGMakerPS2Projects/" + projectName+ "/RolePlayingGamePS2.elf\"";
    system(texty.c_str());
}

void RPGMakerPS2::on_actionOptions_triggered()
{
    Options opts;
    //opts.setProjectName(QString::fromStdString(projectName));
    opts.setModal(true);
    opts.setPaths(pcsxPath,pcsx2Path,SourcePathPS1,SourcePathPS2);

    if(opts.exec() == QDialog::Accepted)
    {
        pcsxPath = opts.pcsxPath;
        pcsx2Path = opts.pcsx2Path;
        SourcePathPS1 = opts.SourcePathPS1;
        SourcePathPS2 = opts.SourcePathPS2;

        std::ofstream configfile ("paths.config");

        if (configfile.is_open())
        {
            configfile << pcsxPath + "\n";
            configfile << pcsx2Path + "\n";
            configfile << SourcePathPS1 + "\n";
            configfile << SourcePathPS2 + "\n";
            configfile.close();
        }
    }
}

void RPGMakerPS2::on_actionAbout_QT_triggered()
{
    QApplication::aboutQt();
}

void RPGMakerPS2::on_actionAbout_triggered()
{

    QMessageBox msgBox;
    msgBox.setText("RPG Maker PS is an Open Source project made for the creation of RPG games using PS2SDK and psn00bsdk.\nThis is a non-commercial project, if you paid for this product, you might have been scammed.\nPlease support the official release of this product and its author Drakonchik.\n0PS2SDK Support: Link\npsn00bsdk Support: Link\nDrakonchik: Link ");
    msgBox.exec();
}

void RPGMakerPS2::on_actionMaterialbase_triggered()
{
    matty.exec();
}

void RPGMakerPS2::on_actionCompile_Only_triggered()
{
    std::string combined = "~/RPGMakerPS2Projects/" + projectName + "/";
    system(combined.c_str());
    system("make");
}

void RPGMakerPS2::on_actionPlaytest_ps2link_triggered()
{
    std::string combined = "cd ~/RPGMakerPS2Projects/" + projectName +"/";
    system(combined.c_str());
    system("make run");
}

void RPGMakerPS2::on_MapList_currentRowChanged(int currentRow)
{
    if(currentRow != -1)
    {
        TileMapHandler.SetTileMap(mapNames[currentRow], projectPath + "Data/Maps");
        ChangeTileSet(QString::fromStdString(matty.GetFilePathFromMB(11,TileMapHandler.TilesetID)));
        UpdateTileMap();
    }
}// Yea Honesty... no clue.

// Right click menu on the Map List. Such as creating a new map
void RPGMakerPS2::on_MapList_customContextMenuRequested(const QPoint &pos)
{
        TileMapListMenu->popup(ui->MapList->viewport()->mapToGlobal(pos));
}

void RPGMakerPS2::CreateNewTileMap()
{

    for(int i = 0; i < matty.counts[11]; i++)
    {
        tileMapForm.AddTilesetIndexes(QString::fromStdString(matty.GetFileNameFromMB(11,i)));
    }


    if(tileMapForm.exec() == QDialog::Accepted)
    {
        TileMapHandler.AddTileMap(tileMapForm.MapName, tileMapForm.width, tileMapForm.height, tileMapForm.tilesetIndex, projectPath + "Data/Maps");
        ui->MapList->addItem(QString::fromStdString(tileMapForm.MapName));
        ChangeTileSet(QString::fromStdString(matty.GetFilePathFromMB(11,tileMapForm.tilesetIndex)));

        mapNames[mapCount] = tileMapForm.MapName;
        mapCount++;

    }
    UpdateTileMap();
}

void RPGMakerPS2::ImportTiledTileMap()
{

}

void RPGMakerPS2::EditSelectedTileMap()
{

}

void RPGMakerPS2::DeleteSelectedTileMap()
{

}

void RPGMakerPS2::ChangeTileSet(QString TileSetPath)
{
    TileSet.clear();

    QImage TilesetImage(TileSetPath);


    tileCountW = TilesetImage.width();
    tileCountW = tileCountW / 32;
    tileCountH = TilesetImage.height();
    tileCountH = tileCountH / 32;

    for(int j = 0; j < tileCountH; j++)
    {
        for(int i = 0; i < tileCountW; i++)
        {
            QImage imagycopy = TilesetImage.copy(i*32,j*32,i*32+32,j*32+32);
            QPixmap pixycopy = QPixmap::fromImage(imagycopy);
            Tile[i*tileCountW + j] = new MapTile(pixycopy, i*32,j*32,i*tileCountW + j,i*tileCountW + j, false);
            TileSet.addItem(Tile[i*tileCountW + j]);
            // Connecting the items to the slots
            connect(Tile[i*tileCountW + j],SIGNAL(changedTilesetTile()),this, SLOT(changeTilesetTileSelection()));

        }
    }


    ui->TileSetGraphicsView->setScene(&TileSet);
    ui->TileSetGraphicsView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    ui->TileSetGraphicsView->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    ui->TileSetGraphicsView->show();
}

void RPGMakerPS2::UpdateTileMap()
{
    TileMap.clear();

    for(int j = 0; j < TileMapHandler.MapHeight; j++)
    {
        for(int i = 0; i < TileMapHandler.MapWidth; i++)
        {
            TileMapItems[i*TileMapHandler.MapWidth + j] = new MapTile(Tile[TileMapHandler.GetTileMapArray(i, j)]->pixy, i*32,j*32,i*tileCountW + j,i*tileCountW + j, true);
            TileMap.addItem(TileMapItems[i*TileMapHandler.MapWidth + j]);
            // Connecting the items to the slots
            connect(TileMapItems[i*TileMapHandler.MapWidth + j],SIGNAL(clickedOnTilemap()),this, SLOT(changeTileInTilemap()));
        }
    }

    ui->TileMapGraphicsView->setScene(&TileMap);
    ui->TileMapGraphicsView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    ui->TileMapGraphicsView->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    ui->TileMapGraphicsView->show();
}

// Change Tile selection in Tileset
void RPGMakerPS2::changeTilesetTileSelection()
{
    Tile[selectedTilesetTileNumber]->isSelected = false;

    for(int j = 0; j < tileCountH; j++)
    {
        for(int i = 0; i < tileCountW; i++)
        {
            if(Tile[i*tileCountW + j]->isSelected == true)
            {
                selectedTilesetTileNumber = i*tileCountW + j;
                ui->LabelTileSelected->setText("Tile Selected: " + QString::number(selectedTilesetTileNumber));
            }

        }
    }
}

void RPGMakerPS2::changeTileInTilemap()
{
    for(int j = 0; j < TileMapHandler.MapHeight; j++)
    {
        for(int i = 0; i < TileMapHandler.MapWidth; i++)
        {
            if(TileMapItems[i*TileMapHandler.MapWidth + j]->isSelected == true)
            {
                TileMapItems[i*TileMapHandler.MapWidth + j]->pixy = Tile[selectedTilesetTileNumber]->pixy;
                TileMapHandler.SetTileMapArray(i*tileCountW + j, selectedTilesetTileNumber);
                TileMapItems[i*TileMapHandler.MapWidth + j]->isSelected = false;
            }

        }
    }
}


void RPGMakerPS2::on_actionSave_triggered()
{

    std::ofstream mapfilename (projectPath + "Data/MapNames.dat");

    if (mapfilename.is_open())
    {
        for(int i = 0; i < mapCount; i++)
        {
            mapfilename << mapNames[i] + "\n";
        }
    }
}
