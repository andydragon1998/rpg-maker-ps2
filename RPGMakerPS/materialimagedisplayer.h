#ifndef MATERIALIMAGEDISPLAYER_H
#define MATERIALIMAGEDISPLAYER_H

#include <QWidget>

namespace Ui {
class materialImageDisplayer;
}

class materialImageDisplayer : public QWidget
{
    Q_OBJECT

public:
    explicit materialImageDisplayer(QWidget *parent = nullptr);
    void setUpImage(QString path);
    ~materialImageDisplayer();


private:
    Ui::materialImageDisplayer *ui;
};

#endif // MATERIALIMAGEDISPLAYER_H
