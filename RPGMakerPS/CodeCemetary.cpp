/*
    for(int i = 0; i < 256; i++)
    {
        actors[i].Name = textData.AddDBString("Name");
        actors[i].Nick = textData.AddDBString("Nick");
        actors[i].Description = textData.AddDBString("You shouldn\'t be able to select this unit! Report this to devs!");
        actors[i].Sprite = textData.AddDBString("default.png");
        actors[i].Battler = textData.AddDBString("default.png");
        actors[i].Portrait = textData.AddDBString("default.png");
        actors[i].weapon = 0;
        actors[i].shield = 0;
        actors[i].helmet = 0;
        actors[i].armour = 0;
        actors[i].accessory = 0;
        actors[i].actorClass = 0;
        actors[i].initialLV = 0;
        actors[i].maximalLV = 99;

        ui->List_Actors->insertItem(i, QString::fromStdString(&textData.data[actors[i].Name]));

        items[i].Name = textData.AddDBString("Name");
        items[i].Icon = 0;
        items[i].Description = textData.AddDBString("You shouldn\'t be able to use this item!Report this to devs!");
        items[i].Scope = 0;
        items[i].Occasion = 0;
        items[i].Animation = 0;
        items[i].TargetAnimation = 0;
        items[i].SE = 0;
        items[i].Event = 0;
        items[i].Price = 0;
        items[i].Consumable = 0;
        items[i].Parameter = 0;
        items[i].ParameterInc = 0;
        items[i].RecoverHPProcent = 0;
        items[i].RecoverHP = 0;
        items[i].RecoverMPProcent = 0;
        items[i].RecoverMP = 0;
        items[i].HitRate = 0;
        items[i].PDEFF = 0;
        items[i].MDEFF = 0;
        items[i].Veriance = 0;

        ui->List_Items->insertItem(i, QString::fromStdString(&textData.data[items[i].Name]));

        spells[i].Name = textData.AddDBString("Name");
        spells[i].Icon = 0;
        spells[i].Description = textData.AddDBString("You shouldn\'t be able to use this spell!Report this to devs!");
        spells[i].Scope = 0;
        spells[i].Occasion = 0;
        spells[i].UserAnimation = 0;
        spells[i].TargetAnimation = 0;
        spells[i].SE = 0;
        spells[i].Event = 0;
        spells[i].MPCost = 0;
        spells[i].Power = 0;
        spells[i].ATKF = 0;
        spells[i].EVAF = 0;
        spells[i].STRF = 0;
        spells[i].DEXF = 0;
        spells[i].AGIF = 0;
        spells[i].INTF = 0;
        spells[i].HitRate = 0;
        spells[i].PDEFF = 0;
        spells[i].MDEFF = 0;
        spells[i].Variance = 0;

        ui->List_Spells->insertItem(i, QString::fromStdString(&textData.data[spells[i].Name]));

        weapons[i].Name = textData.AddDBString("Name");
        weapons[i].Icon = 0;
        weapons[i].Description = textData.AddDBString("You shouldn\'t be able to use this weapon!Report this to devs!");
        weapons[i].AttackAnimation = 0;
        weapons[i].TargetAnimation = 0;
        weapons[i].Price = 0;
        weapons[i].ATK = 0;
        weapons[i].PDEF = 0;
        weapons[i].MDEF = 0;
        weapons[i].STR = 0;
        weapons[i].DEX = 0;
        weapons[i].AGI = 0;
        weapons[i].INT = 0;

        ui->List_Weapons->insertItem(i, QString::fromStdString(&textData.data[weapons[i].Name]));

        armours[i].Name = textData.AddDBString("Name");
        armours[i].Icon = 0;
        armours[i].Description = textData.AddDBString("You shouldn\'t be able to use this armour!Report this to devs!");
        armours[i].Kind = 0;
        armours[i].AutoState = 0;
        armours[i].Price = 0;
        armours[i].EVA = 0;
        armours[i].PDEF = 0;
        armours[i].MDEF = 0;
        armours[i].STR = 0;
        armours[i].DEX = 0;
        armours[i].AGI = 0;
        armours[i].INT = 0;

        ui->List_Armours->insertItem(i, QString::fromStdString(&textData.data[armours[i].Name]));

        enemies[i].Name = textData.AddDBString("Name");
        enemies[i].GraphicBattler = textData.AddDBString("default.png");
        enemies[i].HP = 0;
        enemies[i].MP = 0;
        enemies[i].STR = 0;
        enemies[i].DEX = 0;
        enemies[i].AGI = 0;
        enemies[i].INT = 0;
        enemies[i].ATK = 0;
        enemies[i].PDEF = 0;
        enemies[i].MDEF = 0;
        enemies[i].EVA = 0;
        enemies[i].AttackAnimation = 0;
        enemies[i].TargetAnimation = 0;
        enemies[i].EXP = 0;
        enemies[i].Gold = 0;
        enemies[i].Treasure = 0;
        enemies[i].Chance = 0;
        enemies[i].Action[0] = 0;
        enemies[i].Action[1] = 0;
        enemies[i].Action[2] = 0;
        enemies[i].Action[3] = 0;
        enemies[i].Action[4] = 0;
        enemies[i].Action[5] = 0;
        enemies[i].Action[6] = 0;
        enemies[i].Action[7] = 0;
        enemies[i].Distribution = 0;

        ui->List_Enemies->insertItem(i, QString::fromStdString(&textData.data[actors[i].Name]));

        classy[i].Name = textData.AddDBString("Name");
        classy[i].spells[0][0] = 0;
        classy[i].spells[0][1] = 0;
        classy[i].spells[1][0] = 0;
        classy[i].spells[1][1] = 0;
        classy[i].spells[2][0] = 0;
        classy[i].spells[2][1] = 0;
        classy[i].spells[3][0] = 0;
        classy[i].spells[3][1] = 0;
        classy[i].spells[4][0] = 0;
        classy[i].spells[4][1] = 0;
        classy[i].spells[5][0] = 0;
        classy[i].spells[5][1] = 0;
        classy[i].spells[6][0] = 0;
        classy[i].spells[6][1] = 0;
        classy[i].spells[7][0] = 0;
        classy[i].spells[7][1] = 0;
        classy[i].spells[8][0] = 0;
        classy[i].spells[8][1] = 0;
        classy[i].spells[9][0] = 0;
        classy[i].spells[9][1] = 0;
        classy[i].spells[10][0] = 0;
        classy[i].spells[10][1] = 0;
        classy[i].spells[11][0] = 0;
        classy[i].spells[11][1] = 0;
        classy[i].spells[12][0] = 0;
        classy[i].spells[12][1] = 0;
        classy[i].spells[13][0] = 0;
        classy[i].spells[13][1] = 0;
        classy[i].spells[14][0] = 0;
        classy[i].spells[14][1] = 0;
        classy[i].spells[15][0] = 0;
        classy[i].spells[15][1] = 0;
        classy[i].spells[16][0] = 0;
        classy[i].spells[16][1] = 0;
        classy[i].spells[17][0] = 0;
        classy[i].spells[17][1] = 0;
        classy[i].spells[18][0] = 0;
        classy[i].spells[18][1] = 0;
        classy[i].spells[19][0] = 0;
        classy[i].spells[19][1] = 0;

        ui->List_Classes->insertItem(i, QString::fromStdString(&textData.data[classy[i].Name]));
    }
*/

/*int database::AddDBString(std::string text)
{
    // Checks if string already is in storage
    for(int i = 0; i < textDataLineCount; i++)
    {
        if(!text.compare(textData[i]))
        {
            return i;
        }
    }
    textDataLineCount++;
    textData[textDataLineCount] = text;

    return textDataLineCount;

}*/
 //actors[currentRow].Name = ui->box_ActorName->text();

// All of the following things are to be written in PlayableCharacters.c
// Actors are to be written like this(Line 13):
// Actory[0] = {"", "". "", 0,99,0,0,0,0,0,0,"","","",0,0,true};
// actorStruct = {name,nick,desc,initlv,maxlv,exp,w,s,h,a,ac,sprite,battler,portrait,class,status,isalive(bool)};
// Actor Actory[]; <- Insert actor Ammount Line 10
// extern Actor Actory[]; <- Insert actor Ammount Line 24 gameScreen.c
// Right after insert active members 256 for absence of character
// Line 32 requires case numbers starting from 1 to insert line
// Line 66 requires the status effects per turn in battle.
