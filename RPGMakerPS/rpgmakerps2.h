#ifndef RPGMAKERPS2_H
#define RPGMAKERPS2_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QGraphicsScene>
#include <QObject>

#include "database.h"
#include "materialbase.h"
#include "tilemaps.h"
#include "newtilemap.h"

QT_BEGIN_NAMESPACE
namespace Ui { class RPGMakerPS2; }
QT_END_NAMESPACE

class RPGMakerPS2 : public QMainWindow
{
    Q_OBJECT

public:
    RPGMakerPS2(QWidget *parent = nullptr);
    ~RPGMakerPS2();
    std::string projectName;
    std::string pcsxPath;
    std::string pcsx2Path;
    std::string SourcePathPS1;
    std::string SourcePathPS2;
    std::string projectPath;

    // TileMap and TileSet Graphics Scene Variables
    QGraphicsScene TileMap;
    QGraphicsScene TileSet;

private slots:
    void on_actionDatabase_triggered();

    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionPlaytest_Emulator_triggered();

    void on_actionOptions_triggered();

    void on_actionAbout_QT_triggered();

    void on_actionAbout_triggered();

    void on_actionMaterialbase_triggered();

    void on_actionCompile_Only_triggered();

    void on_actionPlaytest_ps2link_triggered();

    void on_MapList_currentRowChanged(int currentRow);

    void on_MapList_customContextMenuRequested(const QPoint &pos);

    void ImportTiledTileMap();

    void CreateNewTileMap();

    void EditSelectedTileMap();

    void DeleteSelectedTileMap();

    void ChangeTileSet(QString TileSetPath);

    void UpdateTileMap();

    void on_actionSave_triggered();

    // This slot handles the Tileset selection change.
    void changeTilesetTileSelection();

    // This slot handles the Tilemap clicked tile actions
    void changeTileInTilemap();

private:
    Ui::RPGMakerPS2 *ui;

    database datty;
    materialbase matty;
    TileMaps TileMapHandler;
    newTileMap tileMapForm;

    QMenu *TileMapListMenu;

    QAction *NewTileMap;
    QAction *ImportTileMap;
    QAction *EditTileMap;
    QAction *DeleteTileMap;

    QMenu *TileMapEditorMenu;

    QAction *NewEvent;
    QAction *EditEvent;
    QAction *SetPlayerStartPosition;
    QAction *PresetEvents;
};
#endif // RPGMAKERPS2_H
