/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : EDITORS INTERNAL PATHS TO EMULATORS AND ENGINES          */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : options.cpp                                              */
/*  User Interface: options.ui                                               */
/*                                                                           */
/*****************************************************************************/

/* #### QLibrary Header Files #### */
#include <QFileDialog>
#include <QFileInfo>

/* #### User Header Files     #### */
#include "options.h"
#include "ui_options.h"

// Constructor
Options::Options(QWidget *parent) : QDialog(parent),ui(new Ui::Options)
{
    ui->setupUi(this);
}

// Destructor
Options::~Options()
{
    delete ui;
}

void Options::setPaths(std::string pcsx,std::string pcsx2,std::string SourcePS1,std::string SourcePS2)
{
    ui->pcsxPath->setText(QString::fromStdString(pcsx));
    ui->pcsx2Path->setText(QString::fromStdString(pcsx2));
    ui->RPGSourcePath->setText(QString::fromStdString(SourcePS1));
    ui->RPGSourcePathPS2->setText(QString::fromStdString(SourcePS2));
}

void Options::on_pcsxButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,"Select PCSX Emulator executable", "~",
            tr(".AppImage(*.AppImage);;All Files (*)"));
    ui->pcsxPath->setText(fileName);
}

void Options::on_pcsx2Button_clicked()
{
    QString fileName2 = QFileDialog::getOpenFileName(this,"Select PCSX2 Emulator executable", "~",
            tr(".AppImage(*.AppImage);;All Files (*)"));
    ui->pcsx2Path->setText(fileName2);
}

void Options::on_RPGSourceButton_clicked()
{
    QString fileName3 = QFileDialog::getExistingDirectory(this,"Select RPG PS1 source directory");
    ui->RPGSourcePath->setText(fileName3);
}

void Options::on_RPGSourceButtonPS2_clicked()
{
    QString fileName4 = QFileDialog::getExistingDirectory(this,"Select RPG PS2 source directory");
    ui->RPGSourcePathPS2->setText(fileName4);
}

void Options::on_RPGSourceButtonPS_clicked()
{
    QString fileName4 = QFileDialog::getExistingDirectory(this,"Select RPG PS2 source directory");
    ui->RPGSourcePathPS2->setText(fileName4);
}

void Options::on_ConfirmReject_accepted()
{
    pcsxPath = ui->pcsxPath->text().toStdString();
    pcsx2Path = ui->pcsx2Path->text().toStdString();
    SourcePathPS1 = ui->RPGSourcePath->text().toStdString();
    SourcePathPS2 = ui->RPGSourcePathPS2->text().toStdString();
    this->close();
}

