/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : RPG TileMap data handler and Dynamic array housing       */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : tilemaps.cpp                                             */
/*  User Interface:                                                          */
/*                                                                           */
/*****************************************************************************/

/* #### Language Header Files #### */
#include <iostream>
#include <fstream>
#include <sstream>

#include "tilemaps.h"
#include <QtCore>
#include <QtGui>


TileMaps::TileMaps()
{

}

void TileMaps::AddTileMap(std::string MapName, int Width, int Height, int Tileset, std::string Directory)
{
    TileMapName = MapName;
    MapWidth = Width;
    MapHeight = Height;
    TilesetID = Tileset;

    TileMapArraySize = MapWidth * MapHeight;
    std::cout << MapWidth << " " << MapHeight << " " << TilesetID + "";
    TileMapArray1 = (int*) malloc(TileMapArraySize * sizeof(int));
    TileMapArray2 = (int*) malloc(TileMapArraySize * sizeof(int));
    TileMapArray3 = (int*) malloc(TileMapArraySize * sizeof(int));

    // Initialize the Array with 0

    for(int i = 0; i < TileMapArraySize; i++)
    {
        TileMapArray1[i] = 0;  // Tile 0 Value
        TileMapArray2[i] = -1; // Air Value
        TileMapArray3[i] = -1; // Air Value
    }
    std::ofstream MapFile (Directory + "/" + TileMapName + ".csv");

    if (MapFile.is_open())
    {
        MapFile << MapWidth << "," << MapHeight << "," << TilesetID << "\n";

        int sizearray = TileMapArraySize -1;

        for(int i = 0; i < sizearray; i++)
        {
            MapFile << TileMapArray1[i] << ",";

        }
        MapFile << TileMapArray1[sizearray] << "\n";

        for(int i = 0; i < sizearray; i++)
        {
            MapFile << TileMapArray2[i] << ",";
        }
        MapFile << TileMapArray2[sizearray] << "\n";

        for(int i = 0; i < sizearray; i++)
        {
            MapFile << TileMapArray3[i] << ",";
        }
        MapFile << TileMapArray3[sizearray] << "\n";
        MapFile.close();
    }

}

int TileMaps::GetTileMapArray(int i, int j)
{
    int z = i * MapWidth + j;
    return TileMapArray1[z];
}

void TileMaps::SetTileMapArray(int i, int j)
{
    TileMapArray1[i] = j;
}


void TileMaps::SetUpTileset()
{

}


void TileMaps::SetTileMap(std::string MapName, std::string Directory)
{

    std::ofstream MapFile (Directory + "/" + TileMapName + ".csv");

    if (MapFile.is_open())
    {
        MapFile << MapWidth << "," << MapHeight << "," << TilesetID << "\n";
        int sizearray = TileMapArraySize -1;

        for(int i = 0; i < sizearray; i++)
        {
            MapFile << TileMapArray1[i] << ",";

        }
        MapFile << TileMapArray1[sizearray] << "\n";

        for(int i = 0; i < sizearray; i++)
        {
            MapFile << TileMapArray2[i] << ",";
        }
        MapFile << TileMapArray2[sizearray] << "\n";

        for(int i = 0; i < sizearray; i++)
        {
            MapFile << TileMapArray3[i] << ",";
        }
        MapFile << TileMapArray3[sizearray] << "\n";
        MapFile.close();
    }

    std::ifstream LoadMapFile (Directory + "/" + MapName + ".csv");

    std::string line;

    TileMapName = MapName;

    char* data;

    if (LoadMapFile.is_open())
    {
        std::getline(LoadMapFile,line);
        ReadTileMapHeader(line);

        TileMapArraySize = MapWidth * MapHeight;

        std::getline(LoadMapFile,line);
        TileMapArray1 = ReadTileMapLayer(line);

        std::getline(LoadMapFile,line);
        TileMapArray2 = ReadTileMapLayer(line);

        std::getline(LoadMapFile,line);
        TileMapArray3 = ReadTileMapLayer(line);

        ReadTileMapEvents();
        LoadMapFile.close();
    }




}

void TileMaps::ReadTileMapHeader(std::string text)
{

    std::string word;
    std::stringstream stream(text);
    int i = 0;
    while (getline(stream, word, ','))
    {
        switch (i)
        {
            case 0:
                MapWidth = stoi(word);
                i++;
                break;
            case 1:
                MapHeight = stoi(word);
                i++;
                break;
            case 2:
                TilesetID = stoi(word);
                i++;
                break;
            default:

                break;
        }
    }
}

int* TileMaps::ReadTileMapLayer(std::string text)
{
    int* outputArray = (int*) malloc(TileMapArraySize * sizeof(int));

    std::string word;
    std::stringstream stream(text);
    int i = 0;
    while (getline(stream, word, ','))
    {
        outputArray[i]  = stoi(word);
        i++;
    }
    return outputArray;
}

void TileMaps::ReadTileMapEvents()
{

}
