#ifndef DATABASE_H
#define DATABASE_H

#include <QDialog>

namespace Ui
{
class database;

typedef struct
{
    int Name;
    int Nick;
    int Description;
    int initialLV;
    int maximalLV;
    int weapon;
    int shield;
    int helmet;
    int armour;
    int accessory;
    int Sprite;
    int Battler;
    int Portrait;
    int actorClass;
}Actor;

typedef struct
{
    int Name;
    int Icon;
    int Description;
    int Scope;
    int Occasion;
    int UserAnimation;
    int TargetAnimation;
    int SE;
    int Event;
    int MPCost;
    int Power;
    int ATKF;
    int EVAF;
    int STRF;
    int DEXF;
    int AGIF;
    int INTF;
    int HitRate;
    int PDEFF;
    int MDEFF;
    int Variance;
    int ElementList[32];
    int StateChangeList[32];
}Spell;

typedef struct
{
    int Name;
    int Icon;
    int Description;
    int Scope;
    int Occasion;
    int Animation;
    int TargetAnimation;
    int SE;
    int Event;
    int Price;
    int Consumable;
    int Parameter;
    int ParameterInc;
    int RecoverHPProcent;
    int RecoverHP;
    int RecoverMPProcent;
    int RecoverMP;
    int HitRate;
    int PDEFF;
    int MDEFF;
    int Veriance;
    int ElementList[32];
    int StateChangeList[32];
}Item;

typedef struct
{
    int Name;
    int Icon;
    int Description;
    int AttackAnimation;
    int TargetAnimation;
    int Price;
    int ATK;
    int PDEF;
    int MDEF;
    int STR;
    int DEX;
    int AGI;
    int INT;
    int ElementList[32];
    int StateChangeList[32];
}Weapon;

typedef struct
{
    int Name;
    int Icon;
    int Description;
    int Kind;
    int AutoState;
    int Price;
    int EVA;
    int PDEF;
    int MDEF;
    int STR;
    int DEX;
    int AGI;
    int INT;
    int ElementList[32];
    int StateChangeList[32];
}Armour;

typedef struct
{
    int Name;
    int GraphicBattler;
    int HP;
    int MP;
    int STR;
    int DEX;
    int AGI;
    int INT;
    int ATK;
    int PDEF;
    int MDEF;
    int EVA;
    int ElementList[32];
    int StateChangeList[32];
    int AttackAnimation;
    int TargetAnimation;
    int EXP;
    int Gold;
    int Treasure;
    int Chance;
    int Action[8];
    int Distribution;
}Enemy;

typedef struct
{
    int Name;
    int spells[20][2]; // Spell Index and Spell Level learn!
    int EquipableWeapons[256];
    int EquipableArmours[1024];
}Classy;


}

class database : public QDialog
{
    Q_OBJECT

public:
    explicit database(QWidget *parent = nullptr);
    ~database();
    void setProjectName(QString namey, QString pathy);
    void loadData(std::string binFilePaths);

private slots:
    void on_List_Actors_currentRowChanged(int currentRow);

    void on_pushButtonAdd_clicked();

    void on_pushButtonRemove_clicked();

    void on_pushButtonAddItem_clicked();

    void on_pushButtonRemoveItem_clicked();

    void on_List_Items_currentRowChanged(int currentRow);

    void on_pushButtonAddWeapon_clicked();

    void on_ClassSpellTable_cellDoubleClicked(int row, int column);

    void on_pushButtonRemoveSpell_clicked();

    void on_pushButtonAddISpell_clicked();

    void on_List_Spells_currentRowChanged(int currentRow);

    void on_List_Weapons_currentRowChanged(int currentRow);

    void on_List_Armours_currentRowChanged(int currentRow);

    void on_pushButtonAddClass_clicked();

    void on_pushButtonRemoveClass_clicked();

    void on_pushButtonRemoveWeapon_clicked();

    void on_List_Enemies_currentRowChanged(int currentRow);

    void on_pushButtonAddEnemy_clicked();

    void on_pushButtonRemoveEnemy_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_List_Classes_currentRowChanged(int currentRow);

    void on_pushActorFieldSpriteChange_clicked();

    void on_buttonBox_accepted();

    void ErrorDataFiles(int filename, bool isWriting);

    void UpdateDataBaseElements();

    //int AddDBString(std::string text);

private:
    Ui::database *ui;

};

#endif // DATABASE_H
