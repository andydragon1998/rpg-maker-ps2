/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : STRING DATA HANDLER FOR THE RPG ENGINE AND EDITOR        */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : stringblob.cpp                                           */
/*  User Interface:                                                          */
/*                                                                           */
/*****************************************************************************/

/* #### Language Header Files #### */
#include <iostream>
#include <fstream>
#include <string.h>

/* #### User Header Files     #### */
#include "stringblob.h"

using namespace std;

// Constructor
StringBlob::StringBlob()
{
    this->length = 0;
}

int StringBlob::AddDBString(std::string text)
{
    const char *str = const_cast<char*>(text.c_str());

    // Checks if string already is in storage
    for(int i = 0; i < textDataCount; i++)
    {
        if(strcmp(str, &data[offsetDatabase[i]]) == 0)
        {
            return offsetDatabase[i];
        }
    }

    int offset = this->length;
    int length = strlen(str) + 1;
    this->length += length;

    memcpy(&(this->data[offset]), str, length);
    offsetDatabase[textDataCount] = offset;
    textDataCount++;
    return offset;
}
