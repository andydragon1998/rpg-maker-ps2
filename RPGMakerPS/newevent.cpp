/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : RPG EVENT GENERATOR AND EDITOR FOR THE ENGINE            */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : newevent.cpp                                             */
/*  User Interface: newevent.ui                                              */
/*                                                                           */
/*****************************************************************************/

#include "newevent.h"
#include "ui_newevent.h"

NewEvent::NewEvent(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewEvent)
{
    ui->setupUi(this);
}

NewEvent::~NewEvent()
{
    delete ui;
}
