/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : RPG PROJECT FORM FOR NEW USER PROJECT                    */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : newproject.cpp                                           */
/*  User Interface: newproject.ui                                            */
/*                                                                           */
/*****************************************************************************/

/* #### User Header Files     #### */
#include "newproject.h"
#include "ui_newproject.h"

// Constructor
NewProject::NewProject(QWidget *parent) : QDialog(parent),ui(new Ui::NewProject)
{
    ui->setupUi(this);
}

// Destructor
NewProject::~NewProject()
{
    delete ui;
}

void NewProject::on_buttonBox_accepted()
{
    pathText = ui->text_ProjectName->text().toStdString();
    version = ui->playstationVer->currentIndex();
}

void NewProject::on_buttonBox_rejected()
{
    this->close();
}
