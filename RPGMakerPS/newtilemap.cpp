/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : TILE MAP GENERATOR FOR THE EDITOR AND THE ENGINE         */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : newtilemap.cpp                                           */
/*  User Interface: newtilemap.ui                                            */
/*                                                                           */
/*****************************************************************************/


#include "newtilemap.h"
#include "ui_newtilemap.h"

newTileMap::newTileMap(QWidget *parent) : QDialog(parent),ui(new Ui::newTileMap)
{
    ui->setupUi(this);
}

newTileMap::~newTileMap()
{
    delete ui;
}

void newTileMap::on_ConfirmationRejection_accepted()
{
    MapName = ui->TileMapName->text().toStdString();
    width = ui->TileMapWidth->value();
    height = ui->TileMapHeight->value();
    tilesetIndex = ui->ComboTileset->currentIndex();
}

void newTileMap::AddTilesetIndexes(QString TileSetName)
{
    ui->ComboTileset->addItem(TileSetName);
}
