#ifndef MAPTILE_H
#define MAPTILE_H

#include <QPixmap>
#include <QColor>
#include <QGraphicsItem>

class MapTile : public QObject, public QGraphicsItem
{

    Q_OBJECT

public:
    MapTile(const QPixmap pixy, int x, int y, int nr, int id, bool isMapTile);

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) override;

    int TileNr;
    int TileID;
    QPixmap pixy;
    bool isSelected;
    bool isMapTile;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

signals:
    // This is a Tileset selection change signal
    void changedTilesetTile();

    //This is TileMap selection signal
    void clickedOnTilemap();

private:
    int x;
    int y;

};

#endif // MAPTILE_H
